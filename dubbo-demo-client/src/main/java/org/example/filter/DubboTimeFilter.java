package org.example.filter;

import org.apache.dubbo.common.constants.CommonConstants;
import org.apache.dubbo.common.extension.Activate;
import org.apache.dubbo.rpc.*;
import org.example.utils.LoggerUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

@Activate(group = {CommonConstants.CONSUMER})
public class DubboTimeFilter implements Filter {

    private final static Logger LOGGER = LoggerFactory.getLogger(LoggerUtil.class);

    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
        long beginTime = System.currentTimeMillis();
        String reqId = MDC.get(LoggerUtil.REQ_ID);
        reqId = reqId == null ? LoggerUtil.getTrackId() : reqId;
        MDC.put(LoggerUtil.REQ_ID, reqId);
        invocation.setAttachment(LoggerUtil.REQ_ID, reqId);
        Result result;
        try {
            result = invoker.invoke(invocation);
        } finally {
            long endTime = System.currentTimeMillis();
            long resultTime = endTime - beginTime;
            if (resultTime > 20){
                LOGGER.info("dubbo consumer,interface:[{}],beginTime:[{}],time:[{}]", invoker.getInterface(), beginTime, (endTime - beginTime));
            }
            MDC.remove(LoggerUtil.REQ_ID);
        }
        return result;
    }
}
