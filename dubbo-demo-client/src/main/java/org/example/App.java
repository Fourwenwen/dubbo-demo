package org.example;

import org.apache.dubbo.config.ApplicationConfig;
import org.apache.dubbo.config.ReferenceConfig;
import org.apache.dubbo.config.RegistryConfig;
import org.example.common.DemoService;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        ApplicationConfig applicationConfig = new ApplicationConfig();
        applicationConfig.setName("demo-client");

        RegistryConfig registryConfig = new RegistryConfig();
        registryConfig.setAddress("N/A");

        ReferenceConfig<DemoService> reference = new ReferenceConfig<DemoService>(); // 此实例很重，封装了与注册中心的连接以及与提供者的连接，请自行缓存，否则可能造成内存和连接泄漏
        reference.setApplication(applicationConfig);
        reference.setRegistry(registryConfig);
        reference.setInterface(DemoService.class);
// 如果点对点直连，可以用reference.setUrl()指定目标地址，设置url后将绕过注册中心，
// 其中，协议对应provider.setProtocol()的值，端口对应provider.setPort()的值，
// 路径对应service.setPath()的值，如果未设置path，缺省path为接口名
        reference.setUrl("dubbo://192.168.67.76:20880/org.example.common.DemoService");
        reference.setVersion("1.0.1");
        DemoService demoService = reference.get();

        ExecutorService executorService = Executors.newFixedThreadPool(300);

        int num =0;
        while (num<10000){
            for (int i = 0; i < 100; i++) {
                final int i1 = i;
                executorService.submit(()->{
                    demoService.sayHello("world" + i1);
                });
            }
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            num++;
        }

        executorService.shutdown();
        while (!executorService.isTerminated()){
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("结束了");
    }
}
