package org.example;

import org.apache.dubbo.config.ApplicationConfig;
import org.apache.dubbo.config.ProtocolConfig;
import org.apache.dubbo.config.RegistryConfig;
import org.apache.dubbo.config.ServiceConfig;
import org.example.common.DemoService;
import org.example.service.DemoServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Hello world!
 *
 */
public class App 
{
    private final static Logger logger = LoggerFactory.getLogger(App.class);

    public static void main( String[] args ) throws IOException {
        DemoService demoService = new DemoServiceImpl();

        ApplicationConfig application = new ApplicationConfig();
        application.setName("demo-service");

        // 注册中心配置
        RegistryConfig registry = new RegistryConfig();
        registry.setAddress("N/A");

        // 服务提供者协议配置
        ProtocolConfig protocol = new ProtocolConfig();
        protocol.setName("dubbo");
        protocol.setPort(20880);
        protocol.setThreads(100);
        protocol.setHost("127.0.0.1");

        ServiceConfig<DemoService> service = new ServiceConfig<>();
        service.setApplication(application);
        service.setRegistry(registry);
        service.setProtocol(protocol);
        service.setInterface(DemoService.class);
        service.setRef(demoService);
        service.setVersion("1.0.1");
        service.export();
        logger.info("service is started");

        System.in.read();

    }
}
